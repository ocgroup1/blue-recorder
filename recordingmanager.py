# This file is part of Blue Recorder
# Dan Ladds 2020,
# FOSS Project <https://foss-project.com>, 2017, 2018.
#
# Blue Recorder is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Blue Recorder is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Blue Recorder.  If not, see <http://www.gnu.org/licenses/>.
import subprocess, os

from gi.repository import GLib

from logger import Logger
from config import BRConf


"""Manages the actual audio and video recording"""
class RecordingManager:
    DT_WAYLAND = 0
    DT_XORG = 1
    displaytype = DT_WAYLAND

    def __init__(self, app):
        self.app = app
        self.conf = self.app.conf
        self.logger = self.app.logger
        self.recordingVideo = False
        self.recordingAudio = True
        
    def configure(self):
        self.detectDisplay()
        
    def detectDisplay(self):
        # Initially presume XOrg
        self.displayType = self.DT_XORG
        
        # Get size and ref
        self.displayDimensions = subprocess.check_output("xdpyinfo | grep 'dimensions:'|awk '{print $2}'", shell=True)[:-1]
        self.displayRef = os.environ["DISPLAY"]
        
        # Try to identify Wayland
        try:
            if os.environ["XDG_SESSION_TYPE"] == "wayland":
                self.displayType = self.DT_WAYLAND    
        except:
            pass
            
        self.logger.log("Recording on " + ("xorg" if self.displayType == self.DT_XORG else "wayland"), Logger.LOG_LEVEL_INFO) 
        
    def start(self, audioID, area = None):
        self.conf.store()
    
        if(self.displayType == self.DT_WAYLAND):
            self._recordWayland(audioID, area)
        elif(self.displayType == self.DT_XORG):
            self._recordXOrg()
        
    def _recordWayland(self, audioID, area = None):
        
        # Get the Gnome Screencast 
        self.screencast = self.app.bus.get('org.gnome.Shell.Screencast', 
            '/org/gnome/Shell/Screencast')
        
        self.videoPipeline = "vp8enc min_quantizer=10 max_quantizer=50 cq_level=13 cpu-used=5 deadline=1000000 threads=%T ! queue ! webmmux"
        self.audioPipeline = "ffmpeg -f pulse -i " + audioID + "/tmp/blue-recorder.mkv -y"
        
        if(self.conf.recordVideo):
            if area == None:
                self.screencast.Screencast(
                self.conf.destinationPath, {
                    'framerate' : GLib.Variant('i', self.conf.framerate),
                    'draw-cursor' : GLib.Variant('b', self.conf.showMouse),
                    'pipeline' : GLib.Variant('s', self.videoPipeline),
                })
            else:
                self.screencast.ScreencastArea.Screencast(
                area.x, area.y, area.w, area.h,
                {
                    'framerate' : GLib.Variant('i', self.conf.framerate),
                    'draw-cursor' : GLib.Variant('b', self.conf.showMouse),
                    'pipeline' : GLib.Variant('s', self.videoPipeline),
                })
            self.recordingVideo = True
            
        if(self.conf.recordAudio):
            self.audioProcess = subprocess.Popen(audioPipeline)
            self.recordingAudio = True
            
        self.logger.log("Recording started", notifyLog=True)
        
    def _recordXOrg(self):
        pass
        
    def stop(self):
        if(self.displayType == self.DT_WAYLAND):
            self._stopWayland()
        elif(self.displayType == self.DT_XORG):
            self._stopXOrg()
        
        self.logger.log("Recording stopped", notifyLog=True)
        
    def _stopWayland(self):     
        try:
            if(self.recordingVideo):
                self.screencast.StopScreencast()
                self.recordingVideo = False
            
            if self.recordingAudio:
                AudioProcess.terminate()
                self.recordingAudio = False
        except Exception as e:
            self.logger.log('Unable to stop recording by normal means: ' + str(e))
        
        
        tmp = "/tmp/blue-recorder.mkv"
        
        if self.recordingVideo and self.recordingAudio:
            tmp2 = "/tmp/blue-recorder-final.mkv"
            m = subprocess.call(["ffmpeg", "-i", self.conf.destinationPath, "-i", tmp, "-c", "copy", tmp2, "-y"])
            tmp = tmp2
      
        k = subprocess.Popen(["mv", tmp, self.conf.destinationPath])
    
    def _stopXOrg(self):
        pass
    
    


