# This file is part of Blue Recorder
# Dan Ladds 2020,
# FOSS Project <https://foss-project.com>, 2017, 2018.
#
# Blue Recorder is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Blue Recorder is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Blue Recorder.  If not, see <http://www.gnu.org/licenses/>.
from datetime import datetime
from gi.repository import Notify
import gettext

"""Handles error logging

Prints and/or sends a UI notification"""
class Logger:
    LOG_LEVEL_INFO = 0
    LOG_LEVEL_WARN = 1
    LOG_LEVEL_FATAL = 2

    def __init__(self, config):
        self.conf = config
        Notify.init("Blue Recorder")
        self._ = gettext.gettext
    
    """Write a line to the log"""
    def log(self, message, logLevel = None, printLog = True, notifyLog = False):
        logLevel = self.LOG_LEVEL_WARN if logLevel == None else logLevel
        
        logLine = "["
        
        if(logLevel == self.LOG_LEVEL_INFO):
            logLine += self._("INFO") + "] "
        elif(logLevel == self.LOG_LEVEL_WARN):
            logLine += self._("WARNING") + "] "
        elif(logLevel == self.LOG_LEVEL_FATAL):
            logLine += self._("FATAL ERROR") + "] "
        else:
            logLine += "?] "
            self.log(self._("Invalid log level in following line"), self.LOG_LEVEL_WARN)
        
        logLine += self._(message)
        logLine += datetime.now().strftime(' [%Y-%m-%d %H:%M:%S]')
        
        if(printLog):
            print(logLine + "\n")
            
        if(notifyLog):
            Notify.Notification.new(logLine)
        
        if (logLevel == self.LOG_LEVEL_FATAL):
            config.store()
            exit()
    
