# This file is part of Blue Recorder
# Dan Ladds 2020,
# FOSS Project <https://foss-project.com>, 2017, 2018.
#
# Blue Recorder is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Blue Recorder is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Blue Recorder.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Gtk, Gdk, GLib, AppIndicator3 as appindicator
from config import BRConf
from logger import Logger
import gettext, signal, os, subprocess, threading

"""User interface"""
class UIManager:
    def __init__(self, app):
        self.app = app
        self._ = gettext.gettext
        self.conf = self.app.conf
        self.logger = self.app.logger
        self.indicator = None
        self.area = None
        
    def load(self):
        
        # Import the glade file and its widgets.
        self.builder = Gtk.Builder()
        possible_ui_file_locations = [
            "/usr/share/blue-recorder/ui.glade",
            "/usr/local/share/blue-recorder/ui.glade",
            os.path.join(os.path.dirname(__file__), "ui", "ui.glade"),
        ]
        for filename in possible_ui_file_locations:
            if os.path.exists(filename):
                self.builder.add_from_file(filename)
                break
        else:
            logger.log("Did not find ui.glade.  Tried\n  %s"
                     % "\n  ".join(possible_ui_file_locations), Logger.LOG_LEVEL_FATAL)
        
        self.getRefs()
        self.setLabels()
        self.makeConnections()
        self.setProperties()
        self.setAudioSources()

        # Load CSS for Area Chooser.
        style_provider = Gtk.CssProvider()
        
        possible_css_file_locations = [
            "/usr/share/blue-recorder/blue-recorder.css",
            "/usr/local/share/blue-recorder/blue-recorder.css",
            os.path.join(os.path.dirname(__file__), "ui", "blue-recorder.css"),
        ]
        
        for filename in possible_css_file_locations:
            if os.path.exists(filename):
                style_provider.load_from_path(filename)
                break
        else:
            logger.log("Did not find blue-recorder.css.  Tried\n  %s"
                     % "\n  ".join(possible_ui_file_locations), Logger.LOG_LEVEL_FATAL)
        
        Gtk.StyleContext.add_provider_for_screen(Gdk.Screen.get_default(), style_provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)
        
        # Connect the handler to the glade file's objects.
        self.builder.connect_signals(self)
        
    def activate(self):
        # Established - respond to events
        signal.signal(signal.SIGINT, signal.SIG_DFL)
        self.window.show_all()
        Gtk.main()
    
    def getRefs(self):
        # Get references
        self.window = self.builder.get_object("window1")
        self.areaChooser = self.builder.get_object("window2")
        self.aboutDialog = self.builder.get_object("aboutdialog")
        self.folderChooser = self.builder.get_object("filechooser")
        self.filename = self.builder.get_object("filename")
        self.command = self.builder.get_object("command")
        self.formatChooser = self.builder.get_object("format")
        self.audioSource = self.builder.get_object("audiosource")
        self.recordButton = self.builder.get_object("recordbutton")
        self.stopButton = self.builder.get_object("stopbutton")
        self.windowGrabButton = self.builder.get_object("button4")
        self.areaGrabButton = self.builder.get_object("button5")
        self.frameText = self.builder.get_object("label2")
        self. delayText = self.builder.get_object("label3")
        self.commandText = self.builder.get_object("label6")
        self.framerate = self.builder.get_object("framerate")
        self.delay = self.builder.get_object("delay")
        self.framesLabel = self.builder.get_object("frameslabel")
        self.delayLabel = self.builder.get_object("delaylabel")
        self.folderLabel = self.builder.get_object("folderlabel")
        self.commandLabel = self.builder.get_object("commandlabel")
        self.audioSourceLabel = self.builder.get_object("audiosourcelabel")
        self.delayAdjustment = self.builder.get_object("adjustment1")
        self.framesAdjustment = self.builder.get_object("adjustment2")
        self.delayPrefAdjustment = self.builder.get_object("adjustment3")
        self.playButton = self.builder.get_object("playbutton")
        self.videoSwitch = self.builder.get_object("videoswitch")
        self.audioSwitch = self.builder.get_object("audioswitch")
        self.mouseSwitch = self.builder.get_object("mouseswitch")
        self.followMouseSwitch = self.builder.get_object("followmouseswitch")
        self.aboutMenuItem = self.builder.get_object("item2")
        
    def makeConnections(self):
        # Connections
        self.aboutMenuItem.connect("activate", self.showAbout)
        self.window.connect("delete-event", Gtk.main_quit)
        self.areaChooser.connect("delete-event", self.hideOnDelete)
        
    def setLabels(self):
    
        # Labels & Text
        self.window.set_title(self._("Blue Recorder"))
        
        self.aboutMenuItem.set_label(self._("About"))
        self.areaChooser.set_name(self._("AreaChooser"))
        self.filename.set_placeholder_text(self._("File Name.."))
        self.command.set_placeholder_text(self._("Enter your command here.."))
        
        self.formatChooser.append("mkv", self._("MKV (Matroska multimedia container format)"))
        self.formatChooser.append("avi", self._("AVI (Audio Video Interleaved)"))
        self.formatChooser.append("mp4", self._("MP4 (MPEG-4 Part 14)"))
        self.formatChooser.append("wmv", self._("WMV (Windows Media Video)"))
        self.formatChooser.append("gif", self._("GIF (Graphics Interchange Format)"))
        self.formatChooser.append("nut", self._("NUT (NUT Recording Format)"))
        self.formatChooser.set_active(0)
        
        self.videoSwitch.set_label(self._("Record Video"))
        self.audioSwitch.set_label(self._("Record Audio"))
        self.mouseSwitch.set_label(self._("Show Mouse"))
        
        self.followMouseSwitch.set_label(self._("Follow Mouse"))
        
        self.aboutDialog.set_transient_for(self.window)
        self.aboutDialog.set_program_name(self._("Blue Recorder"))
        self.aboutDialog.set_version("4.0.0")
        self.aboutDialog.set_copyright("2020 Dan Ladds, © 2019 M.Hanny Sabbagh")
        self.aboutDialog.set_wrap_license(True)
        self.aboutDialog.set_license("Blue Recorder is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.\n\nGreen Recorder is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.\n\nSee the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with Green Recorder. If not, see <http://www.gnu.org/licenses/>.")
        self.aboutDialog.set_comments(self._("A simple screen recorder for Linux desktop. Supports Wayland & Xorg."))
        self.aboutDialog.set_authors(['M.Hanny Sabbagh <mhsabbagh@outlook.com>','Alessandro Toia <gort818@gmail.com>','Dan Ladds <dan@tech-hogar.es>', 'Patreon Supporters: Ahmad Gharib, Medium,\nWilliam Grunow, Alex Benishek.'])
        self.aboutDialog.set_artists(['Mustapha Assabar'])
        self.aboutDialog.set_website("https://gitlab.com/ocgroup1/blue-recorder")
        self.aboutDialog.set_logo_icon_name("blue-recorder")
        
        self.windowGrabButton.set_label(self._("Select a Window"))
        self.areaGrabButton.set_label(self._("Select an Area"))
        self.frameText.set_label(self._("Frames:"))
        self.delayText.set_label(self._("Delay:"))
        self.commandText.set_label(self._("Run Command After Recording:"))
        self.audioSourceLabel.set_label(self._("Audio Input Source:"))
    
    def showAbout(self, button):
        self.aboutDialog.run()
        self.aboutDialog.hide()
    
    def hideOnDelete(self):
        self.areaChooser.hide()
        return True
    
    """Set default values from configuration"""
    def setProperties(self):
        self.videoSwitch.set_active(self.conf.recordVideo)
        self.audioSwitch.set_active(self.conf.recordAudio)
        self.mouseSwitch.set_active(self.conf.showMouse)
        self.followMouseSwitch.set_active(self.conf.followMouse)
        self.framerate.set_value(self.conf.framerate)
        self.delay.set_value(self.conf.delay)
        self.folderChooser.set_uri(self.conf.folder)
        self.filename.set_text(self.conf.filename)
        self.command.set_text(self.conf.command)
        self.stopButton.set_sensitive(False)
        self.playButton.set_sensitive(False)
        
        self.windowGrabButton.set_sensitive(False)
        self.followMouseSwitch.set_sensitive(False)
        self.formatChooser.remove_all()
  
        self.formatChooser.append("webm", self._("WebM (The Open WebM Format)"))
        self.formatChooser.set_active(0)

    def setAudioSources(self):
        self.audioSource.append("default", self._("Default PulseAudio Input Source"))
        
        try:
          self.audioSourceNames = subprocess.check_output("pacmd list-sources | grep -e device.description", shell=True)
          self.AudioSourceIDs = subprocess.check_output("pacmd list-sources | grep -e device.string", shell=True)
        except Exception as e:
            self.logger.log("Error getting audio sources: " + str(e))
            return
        
        self.audioSourceNames = self.audioSourceNames.decode().split("\n")[:-1]

        for i in range(len(self.audioSourceNames)):
          self.audioSourceNames[i] = self.audioSourceNames[i].replace("\t\tdevice.description = ", "")
          self.audioSourceNames[i] = self.audioSourceNames[i].replace('"', "")
          self.audioSource.append(str(i), self.audioSourceNames[i])
          
        self.audioSource.set_active(0)

    def recordClicked(self, button):
        self.logger.log("Trying to start recording", notifyLog=True)
        
        self.conf.audioSource = self.audioSource.get_active_id()
        self.conf.format = self.formatChooser.get_active_id()
        
        if self.conf.filename == "":
            self.conf.filename == "blue-recorder"
            self.filename.set_text(self.conf.filename)
        
        self.conf.destinationPath = os.path.join(self.conf.folder, self.conf.filename + "." + self.conf.format)
        
        self.app.record()
    
    def checkOverwrite(self):
        if os.path.exists(self.conf.destinationPath):
            dialog = Gtk.Dialog(
                self._("File already exists"),
                None,
                Gtk.DialogFlags.MODAL,
                (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                Gtk.STOCK_OK, Gtk.ResponseType.OK,),
            )
            
            dialog.set_transient_for(self.window)
            dialog.set_default_size(150,100)
            label = Gtk.Label(_("Would you like to overwrite this file?"))
            box = dialog.get_content_area()
            box.add(label)
            dialog.show_all()
            response = dialog.run()
            dialog.destroy()
            
            if response == Gtk.ResponseType.OK:
                dialog.destroy()
                return True
            elif response == Gtk.ResponseType.CANCEL:
                dialog.destroy()
                return False
        else:
           return True
    
    def minify(self):
        self.window.iconify()
        Gdk.flush()
        
        # Establish subthread for tray icon
        with threading.RLock() as trayThread:
            self.indicator = recorderIndicator(self)
    
    def stopRecording(self):
        self.app.stopRecording()
        
        self.window.present()
        self.playButton.set_sensitive(True)
        
    def selectArea(self, button):
        self.areaChooser.set_title(self._("Area Chooser"))
        self.areaChooser.show()
        
    def selectWindow(self, button):
        output = subprocess.check_output(["xwininfo | grep -e Width -e Height -e Absolute"], shell=True)[:-1]
        
    def stopClicked(self, button):
        self.recorder.stop()
        
    def playButtonClicked(self, button):
        subprocess.call(["xdg-open", self.conf.destinationPath])

    def areaSettings(self, GtkButton):
        output = subprocess.check_output(["xwininfo -name \"Area Chooser\" | grep -e Width -e Height -e Absolute"], shell=True)[:-1]
        
        areaaxis = [int(l.split(':')[1]) for l in output.split('\n')]
        
        self.area = {
            "x" : areaaxis[0] + 12,
            "y" : areaaxis[1] + 48,
            "w" : areaaxis[2] - 24,
            "h" : areaaxis[3] - 80,
        }
        
        self.areaChooser.hide()
        self.logger.log("Area position saved", notifyLog=True)

    def framerateChanged(self, spinButton):
        self.conf.framerate = int(self.framerate.get_value())

    def delayChanged(self, spinButton):
        self.conf.delay = int(self.delay.get_value())
          
    def filenameChanged(self, entry):
        conf.filename = urllib.parse.unquote(self.filename.get_text())
               
    def folderSelected(self, fileButton):
        self.conf.folder = urllib.parse.unquote(self.folderChooser.get_uri())
    
    def commandChanged(self, entry):
        self.conf.command = self.command.get_text()
          
    def videoSwitchChanged(self, switch):
        conf.recordVideo = self.videoSwitch.get_active()
          
    def audioSwitchChanged(self, switch):
        conf.recordAudio = self.audioSwitch.get_active()
          
    def mouseSwitchChanged(self, switch):
        conf.showMouse = self.mouseSwitch.get_active()
          
    def followMouseSwitchChanged(self, switch):
        self.conf.followMouse = self.followMouseSwitch.get_active()
        
class recorderIndicator:
    def __init__(self, gui):
        self.gui = gui
        
        self.indicator = appindicator.Indicator.new("Blue Recorder", '/usr/share/pixmaps/blue-recorder.png', appindicator.IndicatorCategory.APPLICATION_STATUS)
        self.indicator.set_status(appindicator.IndicatorStatus.ACTIVE)
        
        self.menu = Gtk.Menu()
        stopRecordingButton = Gtk.MenuItem(self.gui._("Stop Recording"))
        stopRecordingButton.connect('activate', self.stopRecording)
        self.menu.append(stopRecordingButton)
        self.menu.show_all()
        
        self.indicator.set_menu(self.menu)
        self.indicator.set_secondary_activate_target(stopRecordingButton)
        Gtk.main()
    
    def stopRecording(self, button):
        subprocess.call(["sleep", "1"]) # Wait for ffmpeg
        self.gui.stopRecording()
        self.indicator.set_status(appindicator.IndicatorStatus.PASSIVE)
        
