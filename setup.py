#!/usr/bin/python
from setuptools import setup, find_packages
from subprocess import call
from glob import glob
from os.path import splitext, split

data_files = [ ("share/green-recorder", ["ui/ui.glade"]),
                    ("share/pixmaps", ["data/blue-recorder.png"]),
                     ("share/applications", ["data/blue-recorder.desktop"]) ] 

po_files = glob("po/*.po")
for po_file in po_files:
  lang = splitext(split(po_file)[1])[0]
  mo_path = "locale/{}/LC_MESSAGES/green-recorder.mo".format(lang)
  call("mkdir -p locale/{}/LC_MESSAGES/".format(lang), shell=True)
  call("msgfmt {} -o {}".format(po_file, mo_path), shell=True)
locales = map(lambda i: ('share/'+i, [i+'/green-recorder.mo', ]), glob('locale/*/LC_MESSAGES'))

data_files.extend(locales)

setup(name = "BlueRecorder",
      version = "4",
      packages=find_packages
      description = "Screen recorder for Gnome/Wayland",
      author = "Dan Ladds", 
      author_email = "dan@tech-hogar.es",
      url = "https://gitlab.com/ocgroup1/blue-recorder/",
      license='GPLv3',
      scripts=['blue-recorder'],
      data_files=data_files)
