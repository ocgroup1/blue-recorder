# This file is part of Blue Recorder
# Dan Ladds 2020,
# FOSS Project <https://foss-project.com>, 2017, 2018.
#
# Blue Recorder is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Blue Recorder is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Blue Recorder.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import GLib
import configparser, os

"""Manage recorder configuration

The BRConf class manages runtime and persistent config. It serves as a wrapper for the underlying config and programatically fixes
the available options. Configuration file name and path can be overwritten to manage multiple configuration sources

Not backward compatible with Green Recorder as some names have been changed for clarity"""
class BRConf:
    """Number of frames per second (int)"""
    @property
    def framerate(self):
        return int(self.config.get('Options', 'framerate'))
    
    @framerate.setter
    def framerate(self, f):
        self.config.set('Options', 'framerate', str(f))
        self.tainted = True
    
    """Delay in seconds before recording starts (int)"""
    @property
    def delay(self):
        return int(self.config.get('Options', 'delay'))
    
    @delay.setter
    def delay(self, d):
        self.config.set('Options', 'delay', str(d))
        self.tainted = True
    
    """Folder in which to save videos (string)"""
    @property
    def folder(self):
        return self.config.get('Options', 'folder')
        
    @folder.setter
    def folder(self, f):
        self.config.set('Options', 'folder', str(f))
        self.tainted = True
    
    """Command to run after recording (string)"""
    @property
    def command(self):
        return self.config.get('Options', 'command')
        
    @command.setter
    def command(self, c):
        self.config.set('Options', 'command', str(c))
        self.tainted = True
    
    """Filename to save the recording to"""
    @property
    def filename(self):
        return self.config.get('Options', 'filename')
    
    @filename.setter
    def filename(self, f):
        self.config.set('Options', 'filename', str(f))
        self.tainted = True
    
    """Whether to record video or not (bool)"""
    @property
    def recordVideo(self):
        return bool(self.config.get('Options', 'recordvideo'))
        
    @recordVideo.setter
    def recordVideo(self, v):
        self.config.set('Options', 'recordvideo', str(v))
        self.tainted = True
    
    """Whether to record audio or not (bool)"""
    @property
    def recordAudio(self):
        bool(self.config.get('Options', 'recordAudio'))
    
    @recordAudio.setter
    def recordAudio(self, a):
        self.config.set('Options', 'recordaudio', str(a))
        self.tainted = True
    
    """Whether to show the mouse in the video (bool)"""
    @property
    def showMouse(self):
        return bool(self.config.get('Options', 'showMouse'))
        
    @showMouse.setter
    def showMouse(self, m):
        self.config.set('Options', 'showMouse', str(m))
        self.tainted = True
    
    """Whether the video capture area should follow the mouse or not (bool)"""
    @property
    def followMouse(self):
        return bool(self.config.get('Options', 'followmouse'))
        
    @followMouse.setter
    def followMouse(self, f):
        self.config.set('Options', 'followmouse', str(f))
        self.tainted = True
    
    def __init__(self):
        self.config = configparser.ConfigParser()
        self.initialised = False
        self.tainted = False
    
    """Read from config storage"""
    def read(self, confDirName = "blue-recorder", confFileName = "config.ini"):
    
        # Set default values
        videoFolder = GLib.get_user_special_dir(GLib.UserDirectory.DIRECTORY_VIDEOS)
              
        if videoFolder is None:
            videoFolder = os.environ['HOME']
        
        self.config.add_section('Options')
        self.framerate = 30
        self.delay = 0
        self.command = ''
        self.filename = ''
        self.recordVideo = True
        self.recordAudio = True
        self.showMouse = True
        self.followMouse = False
        self.folder = videoFolder
    
        confDir =  os.path.join(GLib.get_user_config_dir(), confDirName)
        self.confFile = os.path.join(confDir + confFileName)
        
        if not os.path.exists(confDir):
              os.makedirs(confDir)

        if os.path.isfile(confFileName):
            self.config.read(self.confFileName)
                
        self.initialised = True
    
    """Write to config storage"""
    def store(self):
        # Don't save a config that's empty or not changed
        if(self.initialised):
            with open(self.confFile, 'w') as cf:
                self.config.write(cf)

