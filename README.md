# Blue Recorder
Blue Recorder is a screen recorder for Gnome/Wayland. It is a fork/partial rewrite of Green Recorder, the popular and powerful screen recorder. The authors of Green Recorder have had to step away from the project. We hope to maintain familiarity for Green Recorder users.

Blue Recorder is written in Python 3 which is now the standard and available on virtually all distributions.

Help is always welcome; particularly in the area of testing on varied distributions and installations. At the moment a Fedora 31 box is the primary development environment.

Requires:
ffmpeg
AppIndicator Support: https://extensions.gnome.org/extension/615/appindicator-support/

## About

A simple desktop recorder for Gnome/Wayland on GNU/Linux. Built using Python 3, GTK+ 3 and ffmpeg.

The following formats are currently supported: **mkv**, **avi**, **mp4**, **wmv**, **gif** and **nut** (And only WebM for Wayland presently, but this should soon be resolved). You can stop the recording process easily by right-clicking the icon and choosing "Stop Record". Or middle-clicking the recording icon in the notifications area (but doesn't work on all interfaces).

You can choose the audio input source you want from the list. You can also set the default values you want by simply changing them in the interface, and the program will save them for you for the next time you open it.

### How it works?

It uses the D-Bus API to connect to the built-in screencasting tool in GNOME Shell. It uses this to record video. To record audio, it launches an instance of ffmpeg in the background. After the recording is finished, it merges the two files into the WebM file.

By default Blue Recorder uses the V8 encoder instead of the default V9 encoder in GNOME Shell because of the CPU & RAM consumption issue with V9. Which - now - should also give you better performance. On Xorg, each format uses its own default encoder.

Also, for GIF format, Blue Recorder first records the required video as a raw video. And then it generated the GIF image from the raw video. In this way, you'll get an optimized GIF image size which is at least 10x better than the normal ffmpeg recording.

### Localization

Blue Recorder supports localization. If you want to translate the program into your language, fork the repository and create a new file under "po" folder with your language ISO code (like fr.po, de.po, cs.po..). And translate the strings from there.

Alternatively, you can open the blue-recorder.pot file using programs like PoEdit and start translating.

    
## License

The program is released under GPL 3.
